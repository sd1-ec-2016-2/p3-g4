var fs = require('fs');
var express = require('express');  // módulo express
var app = express();               // objeto express
var server = require('http').Server(app);
var socketio = require('socket.io')(server);
var path = require('path');     // módulo usado para lidar com caminhos de
server.listen(8080);
var cookieParser = require('cookie-parser');  // processa cookies
app.use(cookieParser());
var bodyParser = require('body-parser');  // processa corpo de requests
app.use(bodyParser.urlencoded( { extended: true } ));
var irc = require('irc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

socketio.listen(server).on('connection', function (socket) {
    socket.on('message', function (msg) {
        console.log('Message Received: ', msg);
	console.log(typeof msg);
	if (msg.substring(0,1) == '/') {
		processar_comando(msg, socket);
	}
        //socket.broadcast.emit('message', msg);
  	// var irc_client = proxies[req.cookies.id].irc_client;
	else {
		var irc_client = proxies[socketio.id].irc_client;
		irc_client.say(irc_client.opt.channels[0], msg );
	}
    });
});

function processar_comando(msg,socket) {
    if ( msg == '/lusers' ) {
	var irc_client = proxies[socketio.id].irc_client;
	var canal = irc_client.opt.channels[0];
	console.log(typeof irc_client);
	console.log('canal: '+canal);
	irc_client.send('names', canal);
    }
}

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;

function proxy(id, servidor, nick, canal) {
// 	var cache = []; // cache de mensagens

	var irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal],});

	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	    /* cache.push(	{"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );*/
	    socketio.emit('message', message);
	});
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	irc_client.addListener('names', function(channel, nicks) {
	  var lista = channel+': '+JSON.stringify(nicks);
  	  console.log('listagem de nicks do canal: '+lista);
	  
	  /* if (dentro_do_canal)
	    client.say(canal, "lista de nicks do canal: "+lista);*/
	});

//	proxies[id] = { "cache":cache, "irc_client":irc_client  };
	proxies[id] = { "irc_client":irc_client  };

	return proxies[id];
}

app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
	//proxy_id++; 
	proxy_id = socketio.id;
	var p =	proxy(	proxy_id,
			req.cookies.servidor,
			req.cookies.nick, 
			req.cookies.canal);
	res.cookie('id', proxy_id);
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  }
});
app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});
